////import path from 'path';
////import favicon from 'serve-favicon';
////import compress from 'compression';
////import helmet from 'helmet';
////import cors from 'cors';

import feathers from '@feathersjs/feathers';
import configuration from '@feathersjs/configuration';
////import express from '@feathersjs/express';



import { Application } from './declarations';
import logger from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import mongoose from './mongoose';
// Don't remove this comment. It's needed to format import lines nicely.

////const app: Application = express(feathers());
const app: Application = feathers(); 

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
////app.use(helmet());
////app.use(cors());
////app.use(compress());
////app.use(express.json());
////app.use(express.urlencoded({ extended: true }));
////app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
////app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
////app.configure(express.rest());


app.configure(mongoose);


// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
////app.use(express.notFound());
////app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);

// Set up Google Cloud Functions entry point
exports.feathers = async (req: any, res: any) => {
  // for CORS
  res.set('Access-Control-Allow-Origin', '*');

  if ( req.method === 'OPTIONS') {
    // Send response to OPTIONS requests for CORS
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Allow-Methods', 'POST, GET, DELETE, OPTIONS');
    res.status(204).send('');

  } else if ( req.method === 'POST' && req.path === '/books') {
    const books = app.service('books');
    const result = await books.create( req.body );
    res.json( result );

  } else if ( req.method === 'GET' && req.path === '/books') {
    const books = app.service('books');
    const result = await books.find();
    res.json( result );

  } else if ( req.method === 'DELETE' && req.path.startsWith('/books/' ) ) {
    const parts = req.path.split('/');
    const books = app.service('books');
    const result = await books.remove(parts[2]);
    res.json( result );

  } else {
    console.log( "method: " + req.method );
    console.log( "path: " + req.path );
    res.send('Unsupported request');
    
  }
};
